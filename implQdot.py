"""
Meaningful Description


CAH Sept 2016
"""
import numpy as np

# Define materials as classes for speed?
class mat_mat0(object):
    '''
    Desc
    '''
    def rho(self,T):   return 1.0
    def cond(self,T):  return 1.0
    def cp(self,T):    return 1.0e2

class mat_mat1(object):
    '''
    Desc
    '''
    def rho(self,T):   return 1.0
    def cond(self,T):  return 1.0
    def cp(self,T):    return 1.0e2


# Implicit Qdot Solver
class implQdot(object):
    '''
    Useful Description
    '''
    def __init__(self, x, Nx, material, norm=1e-5, iterMax=10, iterWarn=4):
        self.x = x                  # np array of spatial coordinate
        self.Nx = Nx                # length of spatial coordinate (kept for easy transition to F77)
        self.material = material    # string of material name
        self.norm = norm            # convergence criteria to account for temperature dep matl prop
        self.iterMax = iterMax      # max iter of solve at each time step
        self.iterWarn = iterWarn    # warn user if more than this many iters are required
        self.a, self.b, self.c, self.d = map(np.zeros_like, (x, x, x, x))   # Work arrays
        self.Txp, self.Tx0, self.Tx1 = map(np.zeros_like, (x, x, x))        # Work arrays
        
        if material==0:              # use select/case or switch/case if in any other language
            self.material = mat_mat0()
        elif material==1:
            self.material = mat_mat1()
        else:
            self.material = mat_mat1()
    
    # Define local methods to simplify access to different material properties
    def rho(self,T):  return self.material.rho(T)
    def cond(self,T): return self.material.cond(T)
    def cp(self,T):   return self.material.cp(T)
    
    def TDMAsolve(self):
        '''
        Thomas Tri Diagonal Matrix Algorithm:      http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
          a*x(i-1) + b*x(i) + c*x(i+1) = d
        Note that this will use arguements as work variables  
        
        https://en.wikibooks.org/wiki/Algorithm_Implementation/Linear_Algebra/Tridiagonal_matrix_algorithm
        '''
        for it in xrange(1, self.Nx):
            mc = self.a[it]/self.b[it-1]
            self.b[it] = self.b[it] - mc*self.c[it-1] 
            self.d[it] = self.d[it] - mc*self.d[it-1]

        xc = self.a
        xc[-1] = self.d[-1]/self.b[-1]
    
        for il in xrange(self.Nx-2, -1, -1):
            xc[il] = (self.d[il]-self.c[il]*xc[il+1])/self.b[il]
    
        return xc

    def calc(self, t, Ts, Nt, qdot):
        '''
        Run calculation for time t[Nt] and surf temps T[Nt]
        '''
        # this is a little lossy, so switch it to an arguement?
        #qdot = np.ones_like(Ts) * np.NaN
        
        # initialize work arrays
        self.Txp[:] = Ts[0]   # previous time step temperature solution
        self.Tx0[:] = Ts[0]   # current time step temperature solution previous iter
        self.Tx1[:] = Ts[0]   # current time step temperature solution current iter
        
        Txp = self.Txp  # do search and replace on these eventually
        Tx0 = self.Tx0
        T   = self.Tx1
        
        #plt.figure()
        for n in range(1,Nt):    # time loop
            Tx0[:] = Txp
            T[:]   = Txp
            for s in range(1,self.iterMax+1):   # convergence loop for space
                for i in range(1,self.Nx-1):   # space loop
                    self.a[i] = 2/(self.x[i+1]-self.x[i-1]) * (                                                         self.cond(0.5*(T[i]+T[i-1]))/(self.x[i]-self.x[i-1]) )
                    self.b[i] = 2/(self.x[i+1]-self.x[i-1]) * ( -self.cond(0.5*(T[i+1]+T[i]))/(self.x[i+1]-self.x[i]) - self.cond(0.5*(T[i]+T[i-1]))/(self.x[i]-self.x[i-1]) ) - self.rho(T[i])*self.cp(T[i])/(t[n]-t[n-1])
                    self.c[i] = 2/(self.x[i+1]-self.x[i-1]) * (  self.cond(0.5*(T[i+1]+T[i]))/(self.x[i+1]-self.x[i])                                         )
                    self.d[i] = -self.rho(Txp[i])*self.cp(Txp[i])*Txp[i]/(t[n]-t[n-1])
                self.a[0]        , self.b[0]        , self.c[0]        , self.d[0]         = ( 0.0, 1.0, 0.0, Ts[n])  # Dirc T
                self.a[self.Nx-1], self.b[self.Nx-1], self.c[self.Nx-1], self.d[self.Nx-1] = (-1.0, 1.0, 0.0, 0.0)    # Neum adiabatic
                
                T = self.TDMAsolve()
                
                # check if another iter is needed of current time step, using inf norm 
                #  of difference in temperatures between iters normalized by previous step
                if (self.norm > np.max(np.abs(Tx0-T)/Txp)):
                    if s>self.iterWarn:
                        print 'iters spent: {:d}'.format(s+1)
                    break
                if s==self.iterMax:
                    print 'max iters meant: {:d}'.format(s+1)
                    
                Tx0[:] = T  # deep copy current solution iter to previous
                
            qdot[n] = self.cond(0.5*(T[1]+T[0])) * (T[0]-T[1])/(self.x[1]-self.x[0])
            #plt.plot(self.x,T,'r-')  # DEBUG plot of profiles as function of time
            Txp[:] = T    # prep for next iter
            
        #plt.show()
        return #qdot
        
    def resCheck(self, t, Ts, Nt, Nxf):
        '''
        Graphical comparison between spatial coordinate resolution
        Nxf - number of nodes in refined solution, using cos() nlin grid spacing
        '''
        import matplotlib.pyplot as plt

        # store current resolution
        x = np.array(self.x)
        Nx = self.Nx
        material = self.material
        
        # nonlin spacing focused at endpoints
        xf = (1.0 - np.cos(np.linspace(0,1,Nxf)*np.pi))/2.0 * x[-1]
        
        # run refined grid
        self.__init__(xf,Nxf,material)
        qdotf = np.ones_like(Ts) * np.NaN
        self.calc(t,Ts,Nt,qdotf)
        
        # restore and run original grid
        self.__init__(x,Nx,material)
        qdot = np.ones_like(Ts) * np.NaN
        self.calc(t,Ts,Nt,qdot)
        
        # plot
        plt.figure()
        plt.subplot(1,2,1)        
        plt.plot(t,qdot,'b-',t,qdotf,'b--')
        plt.title('QDOT - Solid current, Dashed refined')
        
        plt.subplot(2,2,2)        
        plt.plot(t,qdot-qdotf,'b-')
        plt.title('Difference, Raw')
        
        plt.subplot(2,2,4)
        plt.plot(t,(qdot-qdotf)/qdotf,'b-')
        plt.title('Difference, Normalized')
        plt.show()
        
        return
    