"""
Meaningful Description


CAH Sept 2016
"""

import numpy as np
import scipy as scp
import matplotlib.pyplot as plt
import implQdot as iqc


#plt.close('all')

Nx = 20
Nt  = 100
thick = 0.375

# nonlin spacing at origin
dx = np.zeros(Nx)
dx[1] = 0.1     # high activity at front surface, smooth dx transition needed
dx[2] = 0.5     #
dx[3:] = 1      # equispaced web
dx[-1] = 0.1    # low activity at back surface, no need for smooth dx transition
x = np.cumsum(dx)
x *= thick/x[-1]

# linspaceing
#x = np.linspace(0,thick,Nx)

# nonlin spacing focused at endpoints
#x = (1.0 - np.cos(np.linspace(0,1,Nx)*np.pi))/2.0 * thick


t  = np.linspace(0,5,Nt)
Ts = 20.0*scp.special.erf(t) + 80.0 #+ 1.0*(np.random.rand(np.shape(t)[0])-0.5)   # surface temps

iq = iqc.implQdot(x,Nx,1)

# compare resolution needed to resolve features
iq.resCheck(t,Ts,Nt,1000)

'''
qdot = np.ones_like(Ts) * np.NaN
iq.calc(t,Ts,Nt,qdot)

plt.figure()
plt.subplot(1,2,1)        
plt.plot(t,Ts,'b-')
plt.title('Temperature')

plt.subplot(1,2,2)
plt.plot(t,qdot,'b-')
plt.title('QDOT')
plt.show()
'''
   
    
    